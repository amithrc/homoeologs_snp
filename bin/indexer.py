"""
Author: Amith Ramanagar Chandrashekar
Department: Computer science
Module: BWA indexer which incorporates thee position information into header
"""
import argparse
import os
import subprocess

genome = '/data/mcbs913_2018/shared/homoeologs_snp/Chr03_Chr10_genome.fasta'


def runBWA(geno, fasFwd, fasREV, artSAM, tcount=10):
    forwardFastQ = fasFwd[::-1]
    reverseFastQ = ""
    pos = forwardFastQ.find(".")
    forwardFastQ = forwardFastQ[pos + 1:][::-1]
    rev = False

    sam = artSAM[::-1]
    pos = sam.find(".")
    sam = sam[pos + 1:][::-1]

    if fasREV is not None:
        rev = True
        reverseFastQ = fasREV[::-1]
        pos = reverseFastQ.find(".")
        reverseFastQ = reverseFastQ[pos + 1:][::-1]

    if rev == False:
        s = sam + ".filtered.sam"
        out = forwardFastQ + ".annotated"
        com1= r"tr -d '\r' < {0} >{1}".format(artSAM, s)
        print(com1)
        subprocess.run(com1.split())
        com2 = "python3 /data/mcbs913_2018/shared/scripts/full_art_headers.py {0} {1} {2}".format(fasFwd, s, out)
        subprocess.run(com2.split())
        bwacom = "bwa index {0}".format(geno)
        subprocess.run(bwacom.split())
        outFile = out + "bwaannotated.sam"
        bwamem = "bwa mem -t 20 {0} {1} > {2}".format(geno,out, outFile)
        subprocess.run(bwamem.split())

    else:
        s = sam + ".filtered.sam"
        out1 = forwardFastQ + ".annotated"
        os.system(r"tr -d '\r\\n' <{0}> {1} ".format(artSAM, s))
        os.system("python3 /data/mcbs913_2018/shared/scripts/full_art_headers.py {0} {1} {2} -r {3}".format(fasFwd, s, out1,
                                                                                                      fasREV))
        p1 = out1 + ".1.fq"
        p2 = out1 + ".2.fq"
        os.system("bwa index {}".format(geno))
        outBWA = out1 + "BWA_SAM.sam"
        print("Index completed")
        print(p1)
        print(p2)

        # os.system("bwa mem -t 20 {0} {1} {2} > {3}".format(geno,p1,p2,outBWA))


parser = argparse.ArgumentParser(description="Aligner takes the single end or paired end Fastq and runs BWA")
parser.add_argument("fastq", help="ART generated single-end or forward read FastQ")
parser.add_argument("sam", help="ART generated SAM file")
parser.add_argument("-r", help="Reverse end")
args = parser.parse_args()

runBWA(genome, args.fastq, args.r, args.sam)
