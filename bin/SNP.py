"""
Author: Amith Ramanagar Chandrashekar
Department: Computer science
Module: This is the main function starts.
Need to construct by passing the file names and which chromosome
"""
import sys
import chromosomeExtraction as cr
import snpUtils as util
import time


util.printSysInfo()
filename= sys.argv[1]
t1=time.time()
frame1= cr.readCSV(filename,"Chr03")
frame2= cr.readCSV(filename,"Chr10")
seqDict1, headerDict1 = cr.DictBuilder(frame1,"GCA_001683475.1_ASM168347v1_genomic.fna")
seqDict2, headerDict2  = cr.DictBuilder(frame2,"GCA_001683475.1_ASM168347v1_genomic.fna")
cr.Extractor(frame1,seqDict1,headerDict1)
cr.Extractor(frame2,seqDict2,headerDict2)
Chr03 = util.blastExtractor('blast_result1_chr03db_chr10query.csv')
cr.blast_writer(Chr03,'Chr03.fasta','Chr03BLAST')
Chr10 = util.blastExtractor('blast_result2_chr10db_chr03query.csv')
cr.blast_writer(Chr10,'Chr10.fasta','Chr10BLAST')
t2=time.time()
print("Time took {}".format(t2-t1))

