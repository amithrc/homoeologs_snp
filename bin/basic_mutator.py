"""
This very basic mutator takes a file containing a single, complete, ground truth genome (see example test_01) and a
parameter k which is the number of mutations you want to apply to it. Then it creates a new sequence with exactly k
mutations.
Example command line: python basic_mutator.py test_01.fna 1 ALISON
Extended to write VCF: Amith Ramanagar chandrashekar
"""

import random
import heapq
import copy
import re
import snpUtils as util

global filename

headPat = re.compile(r'[a-zA-Z0-9,.]+')


def reverselist(li):
    """
    To sort the integers inside the list
    """
    li.sort()
    return li


def read_file(file_name):
    """
        To the Read the fasta file
    """
    sequence = ''
    header = ''
    name = ''
    with open(file_name, 'r') as my_file:
        for line in my_file:
            if line[0] == '>':
                header = line.rstrip()
                find_name = headPat.search(header)
                name = find_name.group(0)
            else:
                sequence += line.rstrip()
    return sequence, header, name


mutation_model = {
    'A': 'TCG',
    'T': 'CGA',
    'C': 'GAT',
    'G': 'ATC'
}


def sample_positions(sequence, k_snps, seed):
    """
    To create some Random positions to mutate
    """
    random.seed(seed)
    last_position = len(sequence) - 1
    k_random_positions = random.sample(range(last_position), k_snps)
    return k_random_positions


def mutate(sequence, k_random_positions, head):
    """
        Mutates based on the random position lists
    """
    new_sequence = list(sequence)
    sortedlist = reverselist(li=random_positions)
    mc = 0
    with open(filename, "a") as f:
        f.write("#{}\n".format(header))
        for position in sortedlist:
            orig_nucleotide = sequence[position]
            if orig_nucleotide == 'N':
                continue
            mc += 1
            new_nucleotide = random.choice(mutation_model[orig_nucleotide])
            vcf = "ref={0} ,Alt= {1},position={2}\n".format(orig_nucleotide, new_nucleotide, position)
            f.write(vcf)
            new_sequence[position] = new_nucleotide
    return ''.join(new_sequence),mc


def avg_snp_density(sequence, snp_positions, per_bp=1000):
    min_heap = copy.copy(snp_positions)
    heapq.heapify(min_heap)
    end_position = per_bp - 1
    snps = 0
    sum_snp_density = 0.0
    while end_position < len(sequence) + per_bp:
        if len(min_heap) > 0 and min_heap[0] <= end_position:
            heapq.heappop(min_heap)
            snps += 1
        else:
            end_position += per_bp
            sum_snp_density += snps / float(per_bp)
            snps = 0
    return sum_snp_density / (max(len(sequence) / per_bp, 1))


if __name__ == "__main__":
    """
    Takes the fastafile and percentage to mutate and writes to VCF file
    """
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", help="Filename (Fasta Format)")
    parser.add_argument("-s", help="Seed for random generator to produce same output")
    parser.add_argument("-p", help="Percentage changes")

    args = parser.parse_args()
    orig_sequence, header, name = read_file(args.f)
    filename = args.f
    filepos = filename.find(".")
    filename = filename[0:filepos] + "_vcf.vcf"
    perc = float(args.p)
    seqLen = len(orig_sequence)
    totalMutation = int((perc * seqLen) / 100)
    k_snps = totalMutation
    per_bp = 1000
    random_positions = sample_positions(orig_sequence, k_snps, args.s)
    new_sequence,amutated = mutate(orig_sequence, random_positions, header)
    snp_distribution = avg_snp_density(new_sequence, random_positions, per_bp)

    hea = ("{0} | average snp density per {1} bp: {2}, total bp: {3} AM:{4}".format(
        header.replace(name, "{0}.{1}.{2}".format(name, k_snps, args.s)), per_bp, snp_distribution,
        len(new_sequence),amutated))
    se = new_sequence
    filename1 = args.f
    filepos1 = filename1.find(".")
    filename1 = filename1[0:filepos] + "_prime"
    util.WriteToFile(filename1,hea,se)
    '''
    print("{0} | average snp density per {1} bp: {2}, total bp: {3} AM:{4}".format(
        header.replace(name, "{0}.{1}.{2}".format(name, k_snps, args.s)), per_bp, snp_distribution,
        len(new_sequence),amutated))
    print("{0}".format(new_sequence))
    '''

