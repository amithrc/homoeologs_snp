"""
Author: Amith Ramanagar Chandrashekar
Department: Computer science
Module: Collects some over all stats
"""
import platform
import pandas as pd
import csv
from Bio import SeqIO


def printSysInfo():
    """
    Print the System Information
    """
    print(80 * "*")
    print("SNP Homeologus")
    system, node, release, version, machine, processor = platform.uname()
    print("SYSTEM:{}".format(system))
    print("SYSTEM NAME: {}".format(node))
    print("OS RELEASE: {}".format(release))
    print("OS version : {}".format(version))
    print("processor  : {}".format(processor))
    print(80 * "*")


def pandaUniqueFinder(pand):
    """
       Only print the Unique entries in the frame
    """
    dict = {}
    orient = {}
    df = pd.DataFrame()

    for i in range(0, len(pand)):
        ext = pand['scaff_id'][i]

        if ext in dict:
            val = int(dict[ext])
            val += 1
            dict[ext] = val
        else:
            dict[ext] = 1
            orient[ext] = pand['orientID'][i]

    chrList = [pand['chromosome'][0] for i in range(0, len(dict))]
    scaffID = list(dict.keys())
    repeat_count = list(dict.values())
    orientation = list(orient.values())

    df = pd.DataFrame({'chromosome': chrList, 'scaff_id': scaffID, 'repeats': repeat_count, 'orientID': orientation},
                      columns=['chromosome', 'scaff_id', 'repeats', 'orientID'])
    return df


def WriteToFile(filename, header, seq):
    """
       Writes into the fastafile 80 Nucleotides in one line.
       This function is extensively being used from many other modules
    """
    originalLength = len(seq)
    with open(filename + ".fasta", "a") as f:
        f.write(">" + header)
        f.write("\n")

        pos1 = 0
        pos2 = 80
        newLineCount = 0
        len11 = 0
        tempLen = originalLength
        while tempLen >= 0:
            str1 = seq[pos1:pos2]
            f.write(str(str1))
            len11 += 80
            f.write("\n")
            newLineCount += 1
            pos1 += 80
            pos2 += 80
            tempLen -= 80


def blastExtractor(filename):
    """
    Reads the blast file and returns the Scaff_old list
    """
    scaff_set = set()
    scaff_list = list()
    with open(filename, 'r') as f:
        c = 0
        for line in f:
            c += 1
            if c == 1:
                continue
            else:
                scaff_id = line.split("\t")
                scaff_set.add(scaff_id[1])

                if scaff_id[1] in scaff_list:
                    continue
                else:
                    scaff_list.append(scaff_id[1])

    return scaff_list


def upper(filename):
    """
    Takes the fasta as input and creates the newfile tagged as consistent and converts all the Nucleotides to the upper case

    """
    for record in SeqIO.parse(filename, "fasta"):
        # record.seq.upper()
        filepos = filename.find(".")
        file = filename[0:filepos]
        file = file + "_consistent"
        WriteToFile(file, record.description, record.seq.upper())
