"""
Author: Amith Ramanagar Chandrashekar
Department: Computer science
Module: Compare VCF

"""
import os
import argparse
import re
import time
pos =re.compile(r'\d+')

def compare_snps(f1,f2):
    """
    Takes the two Original VCF as f1 and GATK vcf as f2 and checks for how many positions were identified by the GATK.
    The original VCF will hold all the Mutated position information
    """
    actualPos= list()
    foundSNP = 0
    notfoundSNP =0
    c = 0
    with open(f1,'r') as f:
        for line in f:
            if line.startswith("#"):
                continue
            else:
                opos = int(pos.search(line).group(0))
                actualPos.append(opos)

    with open(f2,'r') as f:

        for line in f:
            if line.startswith("#"):
                continue
            else:
                c+=1
                gsplit= line.split()
                gpos = int(gsplit[1])
                gpos = gpos -1

                if gpos in actualPos:
                    foundSNP+=1
                else:
                    notfoundSNP+=1
    '''
    print("Total actual pos {}".format(len(actualPos)))
    print("Found SNP {}".format(foundSNP))
    print("Not found SNP {}".format(notfoundSNP))
    print("Total count in GATK {}".format(c))
    '''
    print(len(actualPos),c,foundSNP,notfoundSNP,(len(actualPos)-foundSNP))



if __name__ == "__main__":
    """
    -g --> GATK VCF file
    -o --> Original VCF 
    """
    parser = argparse.ArgumentParser(description="Comparison VCF")
    parser.add_argument("-o", "--originalvcf", help="Original VCF")
    parser.add_argument("-g", "--gatkvcf", help="GATK vcf")
    args = parser.parse_args()
    origfile = args.originalvcf
    gatkfile = args.gatkvcf
    t1= time.time()
    compare_snps(origfile,gatkfile)
    t2= time.time()
    print("Time took {:2f} sec".format(t2-t1))
