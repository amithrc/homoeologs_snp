"""
Author: Amith Ramanagar Chandrashekar
Department: Computer science
Module: Walker function which walks the entire directory to take the BWA sam file and calls the samparser functionand writes to CSV
"""

import os
import re
import samparser as sam
import pandas as pd
import matplotlib.pyplot as plt

str = 'chr03SE_100bp_5x_error_annotated.sam'
basepair = re.compile(r'(\d\d\d)(\w\w)')

s = '/data/mcbs913_2018/shared/homoeologs_snp/ILLUMINA/'
s1 = 'C:/Users/amith/PycharmProjects/SNP/ILLUMINA'
s3 ='/data/mcbs913_2018/shared/homoeologs_snp/homologs_prime_2percent'

filename = list()
tsam_records = list()
correctA = list()
CQuality = list()
Cqual_0 = list()
Cqual_60 = list()
wAligned = list()
suboptimal = list()
suboptimal_strict = list()
WQuality = list()
incorrect_aligned = list()
ford = list()
revd = list()
subford = list()
subrevd = list()
processed = list()

'''
  return tail, c, CorrectAligned, (Qual_score_average / CorrectAligned), Qual_score_0, Qual_score_60, wronglyAligned, \
           sub_optimal_hits, sub_optimal_strict, (Qual_score_average_wrongAlign / wronglyAligned), (
                   TotalAligned - CorrectAligned), forward_direction, reverse_direction
'''


def walk(cSE, cPE):
    for root, dirs, files in os.walk(s3):
        for name in files:
            if 'annotated' in name:
                if 'sam' in name:

                    ChrID = name.split("_")
                    chrID = ChrID[0]
                    if chrID.upper() == cSE.upper() or chrID.upper() == cPE.upper():
                        print(name)
                        BP = basepair.search(name)
                        print("Name of the file is {} and BP {}".format(name, BP))
                        file = os.path.join(root, name)
                        f, c, tc, cq, q0, q60, wA, sub, substri, wq, ic, fd, rd, sfd, srd = sam.analyzeSam(file, int(
                            BP.group(1)))

                        filename.append(f)
                        tsam_records.append(c)
                        correctA.append(tc)
                        CQuality.append(cq)
                        Cqual_0.append(q0)
                        Cqual_60.append(q60)
                        wAligned.append(wA)
                        suboptimal.append(sub)
                        suboptimal_strict.append(substri)
                        WQuality.append(wq)
                        incorrect_aligned.append(ic)
                        ford.append(fd)
                        revd.append(rd)
                        subford.append(sfd)
                        subrevd.append(srd)

                else:
                    continue


header = "Total_sam_records,Total_correct_aligned,Average_quality_score_correct, Qual_score_0, \
             Qual_score_60, Wrongly_aligned, suboptimal_hits_to_original, suboptimal_strict, \
             Average_quality_wrongly_aligned,IncorrectAlignedSameChromosome\n"

walk('chr03primeSE', 'chr03primeSE')
walk('chr03primePE', 'chr03primePE')
walk('chr10primeSE', 'chr10primeSE')
walk('chr10primePE', 'chr10primePE')

data = pd.DataFrame()

try:
    data = pd.DataFrame({'file': filename, 'total_sam': tsam_records, 'correctly_aligned': correctA,
                         'avergae_quality_correct': CQuality,
                         'quality_0': Cqual_0, 'quality_60': Cqual_60, 'wrongly_aligned': wAligned,
                         'suboptimal': suboptimal, 'suboptimal_strict': suboptimal_strict,
                         'Awrongly_quality_score': WQuality,
                         'incorrect_aligned': incorrect_aligned, 'forward_direction': ford, 'reverse_direction': revd,
                         'suboptimal_forward': subford, 'suboptimal_reverse': subrevd},
                        columns=['file', 'total_sam', 'correctly_aligned', 'avergae_quality_correct', 'quality_0',
                                 'quality_60', 'forward_direction', 'reverse_direction', 'wrongly_aligned',
                                 'suboptimal', 'suboptimal_strict', 'suboptimal_forward', 'suboptimal_reverse',
                                 'Awrongly_quality_score', 'incorrect_aligned'])

except Exception as E:
    print("Error in creating")

print(data)
data.to_csv('homologparamater.csv', sep=',', index=False, mode='w')

'''
ax = data[['total_sam', 'correctly_aligned', 'avergae_quality_correct', 'quality_0',
           'quality_60', 'forward_direction', 'reverse_direction', 'wrongly_aligned',
           'suboptimal', 'suboptimal_strict',
           'Awrongly_quality_score', 'incorrect_aligned', 'suboptimal_forward', 'suboptimal_reverse']].plot(kind='bar',
                                                                                                            title="V comp",
                                                                                                            figsize=(
                                                                                                            15, 10),
                                                                                                            legend=True,
                                                                                                            fontsize=12)
plt.show()
'''