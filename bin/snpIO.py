"""
Author: Amith Ramanagar Chandrashekar
Department: Computer science
Module: Collects some over all stats
"""

from Bio import SeqIO
from Bio.SeqUtils import GC
import numpy
import  os

def snpStats(filename,eachSEQ):

    filenameBase=os.path.basename(filename)
    GCAverage=[]
    AvgSeqLen=[]
    printF = '-'
    for record in SeqIO.parse(filename,"fasta"):
            GCAverage.append(GC(record.seq))
            AvgSeqLen.append(len(record.seq))
            if eachSEQ== True:
                print(printF*75)
                print("GC for {} is {}".format(record.id,GC(record.seq)))
                print("Length of  {} is {}".format(record.id, len(record.seq)))
                print(printF*75)
            else:
                continue

    print("Name of the file  {}".format(os.path.basename(filename)))
    print("Number of records - {}".format(len(GCAverage)))
    print("GC count average - {:.2f}%".format(numpy.average(GCAverage)))
    print("Standard Deviation - {:.2f}%".format(numpy.std(GCAverage)))
    print("Average Length - {:.2f}".format(numpy.average(AvgSeqLen)))








