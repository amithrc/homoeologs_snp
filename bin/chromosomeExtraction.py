"""
Author: Amith Ramanagar Chandrashekar
Department: Computer science
"""

import csv
import pandas as pd
from Bio import SeqIO
import re
import snpUtils as sutils


def readCSV(filename, CharacterPattern):
    """
    This function reads the excel sheets with Chromosome ID and returns the ChromosomeID, Scaffold_id and orientation as Data frame
    """
    with open(filename, 'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        count = 0
        scaffold = []
        orientation = []
        chromeSomeID = []
        res = pd.DataFrame()

        for line in csv_reader:
            if line['Chr'] == CharacterPattern:
                count += 1
                orientation.append(line['Orientation'])
                scaffold.append(line['Chr_scaffold'])
                chromeSomeID.append(line['Chr'])
        try:
            res = pd.DataFrame({'chromosome': chromeSomeID, 'scaff_id': scaffold, 'orientID': orientation},
                               columns=['chromosome', 'scaff_id', 'orientID'])
        except Exception as e:
            print("Error in creating")

        return res


def Extractor(dataF, seqDict, headerDict):
    """
     This takes the Dataframe, Sequence Dictionary and Header dictionary and writes the new Chromosome fasta files
    """
    pat1 = re.compile(r'\w+[_](\d+)')
    file = dataF['chromosome'][0]
    print(80 * "*")
    print("working on {}".format(file))

    for i in range(0, len(dataF)):
        sc = dataF['scaff_id'][i]
        or_id = int(dataF['orientID'][i])

        if or_id == 0 or or_id == 1:
            if sc in seqDict:
                print("Writing...{}".format(sc))
                print("No change in squence when sequence is either 0 or 1, the or_id is {}".format(or_id))
                sutils.WriteToFile(file, headerDict[sc], seqDict[sc])
            else:
                print("Someproblem in Mapping!!!")
        elif or_id == -1:
            if sc in seqDict:
                print("Writing...{}".format(sc))
                print("Reverse complement seq if or_id is {}".format(or_id))
                s = seqDict[sc].reverse_complement()
                sutils.WriteToFile(file, headerDict[sc], s)

            else:
                print("Someproblem in Mapping and Reverse complement!!!")

        else:
            print("Someproblem in Orientation")

    print(80 * "-")


def DictBuilder(dataF1, filename):
    """
    Takes frame and builds the Dictionary which contains header as Key and Sequence as Key.
    The key purpose is to reduce the overall time complexity and compromising on Space.
    """
    tempDict1 = {}
    tempHeader = {}
    temp = dataF1['scaff_id'].tolist()
    tempScaff = []
    cr = dataF1['chromosome'][0]
    pat = re.compile(r'\w+[_](\d+)')

    insidePat = re.compile(r'C[_]\w+[_]\w+[_](\d+)')
    for i in range(0, len(temp)):
        cr_id = pat.match(temp[i]).group(1)
        tempScaff.append('C_Quinoa_Scaffold_' + cr_id)

    count = 0

    for record in SeqIO.parse(filename, "fasta"):
        # sc_id_pos1= record.description.find("C_")
        # sc_id_pos2= record.description.find(",")
        # search=record.description[sc_id_pos1:sc_id_pos2]
        search = insidePat.search(record.description).group(0)

        if search in tempScaff:
            c = insidePat.match(search).group(1)
            tempDict1[cr + "_" + c] = record.seq
            tempHeader[cr + "_" + c] = record.description
            count += 1
            print(cr + "->" + search + "->", count)


        else:
            continue

    return tempDict1, tempHeader


def blast_writer(List, file, fileTag):
    """
    Writes the BLAST output into fasta
    """
    dictT = dict()
    dictH = dict()
    for record in SeqIO.parse(file, "fasta"):
        if record.id in List:
            dictT[record.id] = record.seq
            dictH[record.id] = record.description
        else:
            continue
    for i in List:
        sutils.WriteToFile(fileTag, dictH[i], dictT[i])
