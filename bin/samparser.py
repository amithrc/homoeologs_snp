"""
Author: Amith Ramanagar Chandrashekar
Department: Computer science
Module: SAM parser which collects all the statistics and returns all the stats to the walker function.
"""
import argparse
import re
import time
import os

qnamePat = re.compile(r'([a-zA-Z0-9.]+)-(\d+):(\d+)[:]?(\d+)?')
suboptimal = re.compile(r'XA:\w:(\w+[.]\w+),([-+])(\d+)')

''' Wrongly Aligned Variables'''
wronglyAligned = 0
wAlign_map_q_zero = 0
wAlign_map_q_nonzero = 0
sub_optimal_hits = 0
sub_optimal_strict = 0
Qual_score_average_wrongAlign = 0
Qual_score_average_suboptimal = 0

''' Correctly Aligned Variables'''
CorrectAligned = 0
Qual_score_0 = 0
Qual_score_60 = 0
Qual_score_0_10 = 0
Qual_score_10_20 = 0
Qual_score_20_30 = 0
Qual_score_30_40 = 0
Qual_score_40_50 = 0
Qual_score_50_60 = 0
Qual_score_average = 0
forward_direction = 0
reverse_direction = 0

'''OverAll stats'''
TotalAligned = 0
totalUnaligned = 0
Incorrect_aligned_same_chromosome = 0
c = 0


def analyzeSam(sam, readLen=0):
    """
    This is the main part where it collects all the stas required for paramater sweep.
    This parsing takes care of the correctly aligned, BWA position, primary hits, Secondary hits(Suboptimal hits), Quality score
    Forward reads, reverse reads, suboptimal forward, reverse reads.
    This function is being used by walker.py.
    """
    with open(sam, 'r') as f:
        wronglyAligned = 0
        CorrectAligned = 0
        TotalAligned = 0
        totalUnaligned = 0
        wAlign_map_q_zero = 0
        wAlign_map_q_nonzero = 0
        sub_optimal_hits = 0
        Qual_score_0_10 = 0
        Qual_score_10_20 = 0
        Qual_score_20_30 = 0
        Qual_score_30_40 = 0
        Qual_score_40_50 = 0
        Qual_score_0 = 0
        Qual_score_60 = 0
        Qual_score_50_60 = 0
        Qual_score_average = 0
        Qual_score_average_wrongAlign = 0
        sub_optimal_strict = 0
        Qual_score_average_suboptimal = 0
        c = 0
        Incorrect_aligned_same_chromosome = 0
        forward_direction = 0
        reverse_direction = 0
        suboptimal_forward = 0
        suboptimal_reverse = 0

        for index, line in enumerate(f):
            if line.startswith("@"):
                if 'PG' in line:
                    mat = line.find('CL:')
                    print("BWA Command used --> {}".format(line[mat + 3:]))
                else:
                    continue
            else:
                c += 1
                samline = line.split()
                QNAME = qnamePat.search(samline[0]).group(1)
                origPos = int(qnamePat.search(samline[0]).group(3))
                Flag = int(samline[1])
                RNAME = samline[2]
                bwaPOS = int(samline[3])
                MAPQ = int(samline[4])
                alignedReadsLength = len(samline[9])
                ForwardFlag = False
                ReverseFlag = False

                if Flag == 4:
                    totalUnaligned += 1

                if QNAME == RNAME:
                    TotalAligned += 1

                    if Flag & 0x80:
                        origPos = int(qnamePat.search(samline[0]).group(4))
                        ReverseFlag = True

                    else:
                        ForwardFlag = True

                    w = bwaPOS
                    x = bwaPOS + alignedReadsLength
                    y = origPos
                    z = origPos + readLen

                    if y < x and z > w:
                        CorrectAligned += 1
                        if ForwardFlag:
                            forward_direction += 1
                        elif ReverseFlag:
                            reverse_direction += 1

                        if MAPQ == 0:
                            Qual_score_0 += 1
                            Qual_score_average += MAPQ
                        elif MAPQ == 60:
                            Qual_score_60 += 1
                            Qual_score_average += MAPQ
                        elif MAPQ > 0 and MAPQ < 10:
                            Qual_score_0_10 += 1
                            Qual_score_average += MAPQ
                        elif MAPQ >= 10 and MAPQ < 20:
                            Qual_score_10_20 += 1
                            Qual_score_average += MAPQ
                        elif MAPQ >= 20 and MAPQ < 30:
                            Qual_score_10_20 += 1
                            Qual_score_average += MAPQ
                        elif MAPQ >= 30 and MAPQ < 40:
                            Qual_score_30_40 += 1
                            Qual_score_average += MAPQ
                        elif MAPQ >= 40 and MAPQ < 50:
                            Qual_score_40_50 += 1
                            Qual_score_average += MAPQ
                        elif MAPQ >= 50 and MAPQ < 60:
                            Qual_score_50_60 += 1
                            Qual_score_average += MAPQ
                        else:
                            pass
                else:
                    wronglyAligned += 1
                    Qual_score_average_wrongAlign += MAPQ
                    if MAPQ == 0:
                        wAlign_map_q_zero += 1
                    elif MAPQ > 0:
                        wAlign_map_q_nonzero += 1
                    sub = suboptimal.search(line)
                    if sub is None:
                        pass
                    else:
                        sub = suboptimal.search(line)
                        subOptimalID = sub.group(1)
                        subOptimalpos = int(sub.group(3))

                        if subOptimalID == QNAME:
                            if Flag & 0x80:
                                origPos = int(qnamePat.search(samline[0]).group(4))
                                ReverseFlag = True
                            else:
                                ForwardFlag = True

                            w = subOptimalpos
                            x = subOptimalpos + alignedReadsLength
                            y = origPos
                            z = origPos + readLen

                            if y < x and z > w:
                                sub_optimal_hits += 1
                                if ForwardFlag:
                                    suboptimal_forward += 1
                                elif ReverseFlag:
                                    suboptimal_reverse += 1

                                Qual_score_average_suboptimal += MAPQ
                            else:
                                pass
                        else:
                            sub_optimal_strict += 1

    head, tail = os.path.split(sam)
    print("Name of the file {}".format(tail))
    print("*******************************Wrongly Aligned stats*****************************************")
    print("Total wronglyAligned --> {}".format(wronglyAligned))
    print("Total zero quality score wrongly unaligned -->{}".format(wAlign_map_q_zero))
    print("Total Non zero quality score wrongly unaligned -->{}".format(wAlign_map_q_nonzero))
    print("Number of suboptimal hits going back to actual Chromosome --> {}".format(sub_optimal_hits))
    print("Strict suboptimal hits not going back to actual Chromosome --> {}".format(sub_optimal_strict))
    # print("Total Wrongly aligned Quality score --> {}".format(Qual_score_average_wrongAlign))
    print("Average Wrongly aligned Average Quality score --> {:.2f}".format(
        Qual_score_average_wrongAlign / wronglyAligned))
    #print("Suboptimal hits going back to original chromosome --> {:.2f}".format(
      #  Qual_score_average_suboptimal / sub_optimal_hits))
    print("Suboptimal forward {}".format(suboptimal_forward))
    print("Suboptimal reverse {}".format(suboptimal_reverse))

    print("*******************************Correctly Aligned stats*****************************************")
    print("Total aligned(Going to the same Chromosome) --> {}".format(TotalAligned))
    print("CorrectAligned(Same positon as reads or over lapping position)--> {}".format(CorrectAligned))
    print("Total changes of aligned position--> {}".format(TotalAligned - CorrectAligned))
    print("Quality Score Aligned 0 -->{}".format(Qual_score_0))
    print("Quality Score Aligned 60 -->{}".format(Qual_score_60))
    print("Quality Score Aligned 0-10 -->{}".format(Qual_score_0_10))
    print("Quality Score Aligned 0-10 -->{}".format(Qual_score_0_10))
    print("Quality Score Aligned 10-20 -->{}".format(Qual_score_10_20))
    print("Quality Score Aligned 20-30 -->{}".format(Qual_score_20_30))
    print("Quality Score Aligned 30-40 -->{}".format(Qual_score_30_40))
    print("Quality Score Aligned 40-50 -->{}".format(Qual_score_40_50))
    print("Quality Score Aligned 40-50 -->{}".format(Qual_score_50_60))
    print("Average Quality score (Correctly Aligned -- {:.2f}".format(Qual_score_average / CorrectAligned))
    print("*******************************General stats*****************************************")
    print("Total unaligned -->{}".format(totalUnaligned))
    print("Total SAM records --> {} ".format(c))
    print("Incorrectly aligned within the same chromosome {}".format(TotalAligned - CorrectAligned))
    print("Total number of forward_reads alignment {}".format(forward_direction))
    print("Total number of reverse_reads alignment {}".format(reverse_direction))
    print(80 * "*")

    line1 = tail + "," + str(c) + "," + str(CorrectAligned) + "," + str(
        (Qual_score_average / CorrectAligned)) + "," + str(
        Qual_score_0) + "," + str(Qual_score_60) + "," + str(wronglyAligned) + "," + str(sub_optimal_hits) + "," + str(
        sub_optimal_strict) + "," + str((Qual_score_average_wrongAlign / wronglyAligned)) + "," + str(
        (TotalAligned - CorrectAligned)) + "," + str(forward_direction) + "," + str(reverse_direction) + "\n"

    '''
    header = "Total_sam_records,Total_correct_aligned,Average_quality_score_correct, Qual_score_0, \
             Qual_score_60, Wrongly_aligned, suboptimal_hits_to_original, suboptimal_strict, \
             Average_quality_wrongly_aligned,IncorrectAlignedSameChromosome,forward_direction,reverse_direction\n"

    with open('paramaterssweep.csv', 'a') as csv_file:
        csv_file.write(line1)
    '''

    return tail, c, CorrectAligned, (Qual_score_average / CorrectAligned), Qual_score_0, Qual_score_60, wronglyAligned, \
           sub_optimal_hits, sub_optimal_strict, (Qual_score_average_wrongAlign / wronglyAligned), (
                   TotalAligned - CorrectAligned), forward_direction, reverse_direction,suboptimal_forward,suboptimal_reverse


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Sequence Alignment File Parser")
    parser.add_argument("-s", "--sam", help="Genome file(Reference for BWA")
    parser.add_argument("-r", "--readlength", help="Length of the reads")
    args = parser.parse_args()
    print("Length of the read {}".format(args.readlength))
    read = int(args.readlength)
    t1 = time.time()
    analyzeSam(args.sam, read)
    t2 = time.time()
    print("Time took --> {:.2f}s".format(t2 - t1))
